*** Settings ***
Resource    Exercice9.resource

*** Test Cases ***
 Test formulaire 1
    Given Given je suis connecté sur la page de rdv
    when l'utilisateur rempli le formulaire
    And And je choisi HealthcareProgram et facility    ${Jdd1.HealthcarePogram}    ${Jdd1.facility}
    And je click sur Book appointement
    Then le RDV est confirmé    ${Jdd1.verifHealthcarePogram}    ${Jdd1.facility}

Test formulaire 2
    Given Given je suis connecté sur la page de rdv
    when l'utilisateur rempli le formulaire
    And And je choisi HealthcareProgram et facility    ${Jdd2.HealthcarePogram}    ${Jdd2.facility}
    And je click sur Book appointement
    Then le RDV est confirmé    ${Jdd2.verifHealthcarePogram}    ${Jdd2.facility}

Test formulaire 3
    Given Given je suis connecté sur la page de rdv
    when l'utilisateur rempli le formulaire
    And And je choisi HealthcareProgram et facility    ${Jdd3.HealthcarePogram}    ${Jdd3.facility}
    And je click sur Book appointement
    Then le RDV est confirmé    ${Jdd3.verifHealthcarePogram}    ${Jdd3.facility}

#Test formulaire
#   Given Given je suis connecté sur la page de rdv
 #    when l'utilisateur rempli le formulaire
 #    And je choisi HealthcareProgram    radio_program_medicare
  #   And je click sur Book appointement
   #  then l'utilisateur a pris son RDV

# Test formulaire
 #    Given Given je suis connecté sur la page de rdv
 #    when l'utilisateur rempli le formulaire
 #    And je choisi HealthcareProgram    radio_program_none
  #   And je click sur Book appointement
 #    then l'utilisateur a pris son RDV